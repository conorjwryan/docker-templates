# Docker Compose Templates

I use docker as a means of containerising and testing many services across multiple servers. I needed a means of quickly deploying testing environments across both ARM64/AMD64 based machines.

## How to use this Repository

All of the templates are separated into directories which contain a `docker compose` file, `.env` and an associated `README.md` containing setup instructions/information.

The idea is not to run the containers directly from the directories themselves but to copy them to another part of the system like the following:

1. Clone the Repository onto the machine.

    ```bash
    mkdir docker # good to organise docker associated files  into one folder
    git clone https://gitlab.com/conorjwryan/docker-templates.git templates
    ```

2. Copy the desired folder into another directory:

    ```bash
    cd templates
    cp -R lemp ../php-mail-test # copies the lemp stack into the preceding directory renaming it to something more descriptive
    ```

3. Copy over and edit any associated .env files:

    ```bash
    cp example.env .env
    nano .env
    ```

4. Run the the `up` command

    ```bash
    docker compose up -d
    ```

## Included Scripts

Below is a list of included templates I use for development and testing of apps:

### LEMP Stack

Included in this LEMP stack are the following:

- Nginx 1.25
- PHP 8.3 (via build)
- MariaDB (MySQL database)
- MailCatcher (optional but used for testing mail delivery)

### Project Ara (Dockerised WordPress Environment)

If you are looking for a fully featured dockerised environment to test (or even host) small sites go to [this separate repository](https://gitlab.com/conorjwryan/ara) for more information
