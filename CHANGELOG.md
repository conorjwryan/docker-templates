# CHANGELOG

## 2024-05-01

- #3 Added HTTPS support to the LEMP stack and updated instructions to the README.md file

## 2024-04-20

- #2 - Added improvements/fixed bugs in LEMP stack and updated instructions to the README.md file

## 2024-04-12

- #1 - Initial commit of repo and LEMP stack template.
