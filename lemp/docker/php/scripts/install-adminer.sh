#!/bin/bash

# Define NGINX_WWW_ROOT
declare NGINX_WWW_ROOT

NGINX_WWW_ROOT=$(printenv NGINX_WEB_ROOT)

# Download Latest AdminerEvo From Official Website
curl -sS -o "/tmp/adminer.zip" https://download.adminerevo.org/latest/adminer/adminer-en.zip

# Install Adminer
unzip "/tmp/adminer.zip" -d "${NGINX_WWW_ROOT}/"

# Rename Adminer
mv "${NGINX_WWW_ROOT}/adminer-en.php" "${NGINX_WWW_ROOT}/adminer.php"

echo -e "\nAdminer now installed in ${NGINX_WWW_ROOT}/adminer.php";

rm "/tmp/adminer.zip"