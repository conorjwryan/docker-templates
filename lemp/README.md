# LEMP Stack Docker Compose

This is a simple LEMP stack environment that can be brought up using docker-compose. It consists of the following:

- Nginx 1.25 (http only - assumed to be behind a reverse proxy for https)
- PHP 8.2 (with composer)
- MariaDB (ARM64/AMD64 compatible MySQL Database)
- MailCatcher (for mail delivery testing)
- AdminerEvo (OPTIONAL - for database management - installed via script)

## Setup

Assuming you have already cloned the `docker-templates` repository and are in the root directory do the following:

1. Copy the `lemp` folder to a new directory.

    ```bash
    cp -r lemp /path/to/new/directory
    ```

    It is best practice not to bring up the containers directly in the template repository.

2. Change to the new directory and copy the `.env.example` file to `.env`.

    ```bash
    cd /path/to/new/directory
    cp .env.example .env
    ```

    Edit the `.env` file and set the environment variables as needed.

3. Copy the relevant `nginx` example configuration file into a `.conf` file

    If using `HTTP`:

    ```bash
    cp ./nginx/conf.d/http.conf.example ./nginx/conf.d/http.conf
    ```

    Then skip to step 5.

    If using `HTTPS`:

    ```bash
    cp ./nginx/conf.d/https.conf.example ./nginx/conf.d/https.conf
    ```

4. (OPTIONAL) If using `HTTPS` copy the relevant `cert` and `key` files into the `docker/nginx/certs` directory in the naming format `site.cert.pem` and `site.key.pem`

    ```bash
    cp /path/to/cert.crt ./docker/nginx/certs/site.cert.pem
    cp /path/to/key.key ./docker/nginx/certs/site.key.pem
    ```

5. You can now bring up the LEMP stack using the following command:

    ```bash
    docker-compose up -d
    ```

    It will begin pulling the images from the Docker Hub and begin to build the PHP container.

6. Once the containers are up and running, you can access the web server at `http://localhost:8080` and you should be able to see the `phpInfo()` on the `index.php` page.

7. (OPTIONAL) Add `AdminerEvo` database viewer to the stack by running the following command:

    ```bash
    docker exec -it example_php /scripts/install-adminer.sh
    ```

    *Where example is the name of the project

    You can access the AdminerEvo interface at `http://localhost:8080/adminer.php`.

## Environment Variables

The following environment variables can be used to configure the LEMP stack:

| Variable | Description |
| --- | --- |
| PROJECT_NAME | The name of the project (used for the container names) |
| MARIADB_USER | eg: app_worker |
| MARIADB_PASSWORD | password for the user |
| MARIADB_ROOT_PASSWORD | root password |
| MARIADB_DATABASE | database name |
| NGINX_WEB_PORT | default: `8080` |
| NGINX_WEB_PORT_SSL | default: `4430` |
| NGINX_WEB_ROOT | default: `/var/www/html` |
| MAIL_WEB_PORT | Mail (mailcatcher) |

## Notes

### PHP Container Needs to be Built

The `php` container is built from the official `PHP` image and takes a bit of time to build. This is because it installs a number of PHP extensions that are commonly used in web applications including `composer`.

### AdminerEvo

The `AdminerEvo` database viewer is an optional component that can be added to the stack. It is a single PHP file that can be used to view and manage databases. It is not recommended to use this in production environments.
